import random

import os

def limpiarPantalla ():
    if os.name == "posix":
       os.system ("clear")
       
def dificultad(valor):
    if valor == 1:
        return 6;
    elif valor == 2:
        return 8;
    else:
        return 10;    

def balaPosi(dificultad):
        bala = random.randrange(1, dificultad)
        return bala

def disparo():
    disparo = ("....._|____________________,,__\n..../ `--|||||||||||------------oOOD_]\n.../_==o ____________________|\n.....),---.(_(__) / \n....// () ),------\n...//___//\n../`----' / ...\n./____ / ... .\n")
    print(disparo)

def revisionPartida():
    revision = True
    while (revision == True):
        print()
        opcion = input("Elige una opcion: \n1. Halar Gatillo \n2. Te Arrepientes \n3. Salir de la Partida\n")
        if (opcion != "1") and (opcion != "2") and (opcion != "3"):   
            print()
            print("Debes ingresar un numero de las opciones, vuelve a intentar.") 
            print()
          
        else:
            return opcion 
            revision = False

def pistola():
    pistola = ("....._|____________________,,__\n..../ `--|||||||||||----------------------_]\n.../_==o ____________________|\n.....),---.(_(__) / \n....// () ),------\n...//___//\n../`----' / ...\n./____ / ... .\n")
    return print(pistola)

def cartuchoElegido(dificultad):
    cartuchoElegido = random.randrange(1, dificultad)
    return cartuchoElegido

def partida(dificultad, nombre):
    limpiarPantalla()
    numPerdida = 0
    numAbandono = 0
    numGanada = 0
    print (("el valor de dificultad es: " + str(dificultad)).center(50, "*"))
    print()
    print ("Cargando el arma...")
    pistola()

    bala = balaPosi(dificultad)
    eleccionCar = cartuchoElegido(dificultad)

    cartuchosDisponibles = dificultad
    jugar = True
   
    while (jugar == True):

        opcion = revisionPartida()

        if opcion == "1":
            if bala ==  eleccionCar:
                limpiarPantalla()
                print("Losiento " + nombre+", Has perdidio")
                print()
                disparo()
                numPerdida = numPerdida + 1
                print
                jugar = False               

            elif bala != eleccionCar:
                limpiarPantalla()
                print("Muy bien "+ nombre +", sobreviviste continuemos con la ronda")
                
        elif opcion == "2":
            if bala == eleccionCar:
                limpiarPantalla()
                print("Muy bien "+ nombre +", Descubriste la posicion de la bala. Ganaste la Partida")
                numGanada = numGanada + 1
                jugar = False  
             
            
            elif bala != eleccionCar:
                limpiarPantalla()
                print("Losiento " + nombre+", Has perdidio")
                numPerdida = numPerdida + 1
                jugar = False               

        else:
            limpiarPantalla()
            jugar = False
            numAbandono = numAbandono + 1
            
        if dificultad != eleccionCar:
             eleccionCar = eleccionCar + 1  
            
        else:
            eleccionCar = 1
        
    return numGanada, numPerdida, numAbandono
    print()
    
def iniciar_inst():
    print(("Hola te presento las reglas del juego:").center(50, "*"))
    print()
    print("1. Se presentan 3 niveles:\n   Facil, con revolver de 6 cartuchos\n   Medio, con revolver 8 cartuchos\n   Dificil, con revolver 10 cartuchos")
    print("2. En cada turno tendras 3 opciones:\n  Halar Gatillo, si te topas con la bala Pierdes, sino continua la partida\n  Te Arrepientes, siginifica no disparar. Si el siguiente cartucho no tenia la bala Pierdes, de lo contrario Ganas\n  Abandonar la Partida, abandono de la partida actual")
    print("3. Al iniciar otra partida tendras la sumatoria de tu desempeno de las partidas que hayas jugado")
    print("4. Al salir del juego te mostrara tu desempeno total en las partidas")

def presentacion():
    presentacion = True 
    while presentacion == True:
        print()
        print ("BIENVENIDO AL JUEGO DE LA RULETA RUSA".center(50, "="))
        respuesta = input("Ingresa una opcion: \n1. Jugar\n2. Instruciones del Juego\n")

        if respuesta == "":
            print()
            print ("Ingresa una opcion. Por favor vuelve a intentarlo. ")
            print()

        elif respuesta != "2" and respuesta != "1":
            print()
            print ("Ingresa una opcion. Por favor vuelve a intentarlo. ")
            print()
            
        elif respuesta == "2":    
            iniciar_inst()
           

        else:   
            menu = True
            limpiarPantalla()
            while (menu == True):
                nombre = input("Por favor ingresa tu nombre: ")
                if nombre != "":
                    menu = False
                    return nombre
                    
                else:
                    limpiarPantalla()
                    print ("No ingresate tu nombre. Por favor vuelve a intentarlo. ")
                   
            presentacion = False   
            
def nivelDificultad():
    limpiarPantalla()
    nivelDificultad = True
    while nivelDificultad == True:
        print ("Por favor ingrese el numero, de la dificultad que desea jugar: ")
        try:
            valor = int (input("1. Facil\n2. Medio \n3. Dificil\n"))
            if (valor != 1) and (valor != 2) and (valor != 3):   
                print()
                print("Debes ingresar un numero de las opciones, vuelve a intentar.") 
                print()
                nivelDificultad = True
            else:
                nivelDificultad = False
                return valor 
        except:
            print("Debe ingresar un numero, por favor vuelva a intentar")
            print()
            nivelDificultad = True      
   
nombreJugador = presentacion()

jugar = True
ganadas = 0
perdidas = 0
abandono = 0

while(jugar == True):
    valor = nivelDificultad()
    dificultadDato = dificultad(valor)
    resumen = partida(dificultadDato, nombreJugador)     
    
    control = True
    while (control == True):
        opcion2 = input(nombreJugador+" Elige una opcion: \n1. Jugar otra ronda\n2. Salir\n")

        if opcion2 == "":
                print()
                print ("Ingresa una opcion. Por favor vuelve a intentarlo. ")
                print()

        elif opcion2 != "2" and opcion2 != "1":
            print()
            print ("Ingresa una opcion. Por favor vuelve a intentarlo. ")
            print()

        else:
            if opcion2 == "1":
                ganadas = int(resumen[0]) + ganadas
                perdidas = int(resumen[1]) + perdidas
                abandono = int(resumen[2]) + abandono
                control = False
            else:
                ganadas = int(resumen[0]) + ganadas
                perdidas = int(resumen[1]) + perdidas
                abandono = int(resumen[2]) + abandono
                limpiarPantalla()
                print ("Resultados de Partidas:\nPatidas Ganadas: "+str(ganadas)+"     "+"Partidas Perdidas: "+str(perdidas)+"    "+"Partidas Abandonadas: "+str(abandono))
                control = False
                jugar = False

        

       
    
