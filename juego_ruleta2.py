import random

import os

DIFICULTAD_FACIL = 1
DIFICULTAD_MEDIA = 2

CARGADOR_FACIL = 6
CARGADOR_MEDIA = 8
CARGADOR_DIFICL = 10

def limpiarPantalla ():
    if os.name == "posix":
       os.system ("clear")
    else:
        os.system("cls")
       
def tamanoCargadorRevolver(valor):
    if valor == DIFICULTAD_FACIL:
        return CARGADOR_FACIL;
    elif valor == DIFICULTAD_MEDIA:
        return CARGADOR_MEDIA;
    else:
        return CARGADOR_DIFICL;    

def posicionBala(tamanoCargador):
        bala = random.randrange(1, tamanoCargador)
        return bala

def dibujarDisparo():
    disparo = ("....._|____________________,,__\n" +
    "..../ `--|||||||||||------------oOOD_]\n" +
    ".../_==o ____________________|\n" +
    ".....),---.(_(__) / \n" +
    "....// () ),------\n" +
    "...//___//\n" +
    "../`----' / ...\n" +
    "./____ / ... .\n")
    print(disparo)

def dibujarPistola():
    pistola = ("....._|____________________,,__\n" +
    "..../ `--|||||||||||----------------------_]\n" +
    ".../_==o ____________________|\n" +
    ".....),---.(_(__) / \n" +
    "....// () ),------\n" +
    "...//___//\n" +
    "../`----' / ...\n" +
    "./____ / ... .\n")
    return print(pistola)

def giroTamborPistola(tamañoDelRevolver):
    cartuchoElegido = random.randrange(1, tamañoDelRevolver)
    return cartuchoElegido

def esOpcionValida(opcion, valoresValidos):
    return opcion in valoresValidos

def mostrarOpcionesYValidar(valoresOpciones, textoOpciones):
    mensaje = ("Elige una opcion: \n")
    for valor, texto in zip(valoresOpciones, textoOpciones):
        mensaje = mensaje + valor + ". " + texto + "\n"
    opcion = input(mensaje) 
    if esOpcionValida(opcion, valoresOpciones):
        return opcion
    limpiarPantalla()
    print("Debes ingresar una opcion valida, vuelve a intentar")
    print()
    return mostrarOpcionesYValidar(valoresOpciones, textoOpciones)     
       
def validarAccionDelJuego():
    valoresOpciones = ["1", "2", "3"]
    textoOpciones = ["Halar gatillo", "Te arrepientes", "Abandonar la partida"]
    return mostrarOpcionesYValidar(valoresOpciones, textoOpciones)
    ##return mostrarOpcionesYValidar(["1", "2", "3"], ["Halar gatillo", "Te arrepientes", "Abandonar la partida"] )
     
def validarNivelDificultadDelJuego():
    valoresOpciones = ["1", "2", "3"]
    textoOpciones = ["Facil", "Medio", "Dificil"]
    return mostrarOpcionesYValidar(valoresOpciones, textoOpciones)

def validarOpcionesPresentacionDelJuego():
    valoresOpciones = ["1", "2"]
    textoOpciones = ["Jugar", "Instruciones del Juego"]
    return mostrarOpcionesYValidar(valoresOpciones, textoOpciones)

def validarOpcionesFinalDelJuego():
    valoresOpciones = ["1", "2"]
    textoOpciones = ["Jugar otra ronda", "Salir"]
    return mostrarOpcionesYValidar(valoresOpciones, textoOpciones)
    
def partida(dificultad, nombre):
    limpiarPantalla()

    RESULTADO_VICTORIA = 0
    RESULTADO_DERROTA_DISPARO = 1
    RESULTADO_DERROTA_ABANDONO = 2
       
    print (("el valor de dificultad es: " + str(dificultad)).center(50, "*"))
    print()
    print ("Cargando el arma...")
    dibujarPistola()

    bala = posicionBala(dificultad)
    eleccionCar = giroTamborPistola(dificultad)

    cartuchosDisponibles = dificultad

    while (True):
        
        opcion = validarAccionDelJuego()
        
        if opcion == "1":
            if bala ==  eleccionCar:
                limpiarPantalla()
                print("Lo siento " + nombre+", Has perdidio")
                print()
                dibujarDisparo()  
                return RESULTADO_DERROTA_DISPARO
                            

            elif bala != eleccionCar:
                limpiarPantalla()
                print("Muy bien "+ nombre +", sobreviviste continuemos con la ronda")
                print()
                
        elif opcion == "2":
            if bala == eleccionCar:
                limpiarPantalla()
                print("Muy bien "+ nombre +", Descubriste la posicion de la bala. Ganaste la Partida")
                print()
                return RESULTADO_VICTORIA
            
            elif bala != eleccionCar:
                limpiarPantalla()
                print("Lo siento " + nombre+", Has perdidio")
                print() 
                return RESULTADO_DERROTA_DISPARO                             

        else:
            limpiarPantalla()
            return RESULTADO_DERROTA_ABANDONO
        
        if dificultad != eleccionCar:
             eleccionCar = eleccionCar + 1  
            
        else:
            eleccionCar = 1
        
def mostrarInstrucciones():
    print(("Hola te presento las reglas del juego:").center(50, "*"))
    print()
    print("1. Se presentan 3 niveles:\n   Facil, con revolver de 6 cartuchos\n   Medio, con revolver 8 cartuchos\n   Dificil, con revolver 10 cartuchos")
    print("2. En cada turno tendras 3 opciones:\n  Halar Gatillo, si te topas con la bala Pierdes, sino continua la partida\n  Te Arrepientes, siginifica no disparar. Si el siguiente cartucho no tenia la bala Pierdes, de lo contrario Ganas\n  Abandonar la Partida, abandono de la partida actual")
    print("3. Al iniciar otra partida tendras la sumatoria de tu desempeno de las partidas que hayas jugado")
    print("4. Al salir del juego te mostrara tu desempeno total en las partidas")

def presentacion():
    presentacion = True 
    while presentacion == True:
        print()
        print ("BIENVENIDO AL JUEGO DE LA RULETA RUSA".center(50, "="))
        respuesta = validarOpcionesPresentacionDelJuego()
        
        if respuesta == "1":
            menu = True
            limpiarPantalla()
            while (menu == True):
                nombre = input("Por favor ingresa tu nombre: ")
                if nombre != "":
                    menu = False
                    return nombre
      
                else:
                    limpiarPantalla()
                    print ("No ingresate tu nombre. Por favor vuelve a intentarlo. ")
                    print()
                   
            presentacion = False  

        elif respuesta == "2":
            mostrarInstrucciones()
                
nombreJugador = presentacion()

jugar = True
ganadas = 0
perdidas = 0
abandono = 0
resultados = [0, 0, 0]

while(jugar == True):
    valor = int(validarNivelDificultadDelJuego())
    nivelDificultad = tamanoCargadorRevolver(valor)
    resultadoPartida = partida(nivelDificultad, nombreJugador)
    resultados[resultadoPartida] = resultados[resultadoPartida] + 1
    opcion = validarOpcionesFinalDelJuego()

    if opcion == "2":
        print("Resultados Partida\n" + "Ganadas: " + str(resultados[0])+ "   Perdidas: "+ str(resultados[1])+ "  Abandonadas: " + str(resultados[2]))
        jugar = False